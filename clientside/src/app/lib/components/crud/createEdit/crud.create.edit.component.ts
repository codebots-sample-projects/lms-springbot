/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Store} from '@ngrx/store';
import {tap} from 'rxjs/operators';
import {AbstractModelAudit, RouterState} from 'src/app/models/model.state';
import {getRouterState} from 'src/app/models/model.selector';
import {FileModel} from 'src/app/lib/models/file.model';
import {AbstractComponent} from '../../abstract.component';
import {ButtonStyle, ButtonAccentColour, ButtonSize, IconPosition} from '../../button/button.component';
import {TextfieldType} from '../../textfield/textfield.component';
import {AbstractModel, ModelProperty, ModelRelation, ModelRelationType, ModelPropertyType} from 'src/app/lib/models/abstract.model';
import {ElementType} from '../../abstract.input.component';

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

export interface ActionButtonGroup {
	elements: BottomActionButtonElement[];
}

/**
 * An object used to configure an element within a group.
 */
export interface BottomActionButtonElement {
	className?: string;
	label?: string;
	iconClasses?: string;
	iconPos?: IconPosition;
	buttonStyle?: ButtonStyle;
	buttonAccentColour?: ButtonAccentColour;
	buttonSize?: ButtonSize;
	callback?: ($event: any) => void;
	doHide?: boolean | (() => boolean);
	[s: string]: any;
}

/**
 * An object used for grouping attributes together within the crud edit page
 * 
 * The model and form attributes are optional because they are only required when trying to add additional entities to the crud page, so
 * if you are only grouping attributes of the same entity then do not set them as it can cause unexpected issues.  At the same time, make sure
 * both of them are set, as only setting one will potentially cause issues.
 *
 * The attribute groups are populated through inputting model relations/properties from crud edit components, within this component these are built
 * in separateForm()
 */
export interface AttributeGroup {
	id: string; // The id of the group, needed in the template to determine which group should be removed when removing additional entities in multi crud many relations
	displayName: string; // The name of the group, shown on the page to differentiate the groups, does not need to be unique in multi crud mode with a many relation
	model?: any; // The entity which is being modified in the group, only used in multi crud pages
	form?: FormGroup; // The form group which stores the changes to be made to the model, again only used in multi crud pages
	manyRelation: boolean; // Whether this group represents an additional entity in a many relation
	props: ModelProperty[]; // The properties belonging to this group, only used for populating the combined array
	relations: ModelRelation[]; // The Relations belonging to this group, only used for populating the combined array
	combined: any[]; // The combined props and relations for this group, used for display in the template, and done like this so that order can be mixed up with props and relations in any order
}

@Component({
	// % protected region % [Modify the existing component configurations here] off begin
	selector: 'cb-crud-create-edit',
	templateUrl: './crud.create.edit.component.html',
	// % protected region % [Modify the existing component configurations here] end
	styleUrls: [
		'./crud.create.edit.component.scss',
		// % protected region % [Add any additional SCSS imports here] off begin
		// % protected region % [Add any additional SCSS imports here] end
	],
	// % protected region % [Add any additional component configurations here] off begin
	// % protected region % [Add any additional component configurations here] end
})
export class CrudCreateEditComponent<E extends AbstractModel, T extends AbstractModelAudit<E>> extends AbstractComponent implements OnInit, OnChanges {
	buttonStyle = ButtonStyle;
	buttonAccentColour = ButtonAccentColour;
	buttonSize = ButtonSize;
	iconPos = IconPosition;
	modelRelationType = ModelRelationType;
	elementType = ElementType;
	textFieldType = TextfieldType;

	/**
	 * Element groups to be displayed in the bottom of the action button groups.
	 */
	actionButtonGroups: ActionButtonGroup[];

	/**
	 * Additional Element groups passed in to display unique crud page actions
	 */
	@Input()
	customGroups: ActionButtonGroup[] = [];

	/**
	 * Model to be edited if this component is opened in edit mode.
	 * The value of the model would be applied to the form group
	 */
	@Input()
	model: E;

	/**
	 * Form group to display. This is required to passed in
	 */
	@Input()
	modelFormGroup: FormGroup;

	/**
	 * Event emitter used to trigger events whenever the user clicks on the `Create` or `Save` button.
	 */
	@Output('createOrSaveClick')
	createOrSaveEventEmitter: EventEmitter<{ isCreate: boolean, payload?: { [key in keyof E]?: any }, additionalEntities?: any }> = new EventEmitter();

	/**
	 * Event emitter used to to trigger events when the user wants to switch from the view mode to edit mode.
	 */
	@Output('switchEdit')
	switchEditEventEmitter: EventEmitter<null> = new EventEmitter();

	/**
	 * Event emitter used to trigger events whenever the user clicks on the `Cancel` button.
	 */
	@Output('cancelClick')
	cancelEventEmitter: EventEmitter<null> = new EventEmitter();

	/**
	 * Event emitter used to trigger events whenever the user clicks on the `View History` button.
	 */
	@Output('viewHistory')
	viewHistoryEventEmitter: EventEmitter<null> = new EventEmitter();

	/**
	 * Event emitter used for triggering changes to the displayed list of additional entities (if any) when
	 * using the crud tile in multi entity mode with a 1:M relation
	 */
	@Output('multiEntity')
	multiEntityEventEmitter: EventEmitter<{ groupId: string, entityName: string, payload?: { [key: string]: any } }> = new EventEmitter();

	/**
	 * List of properties/attributes of the model class managed by this create CRUD component.
	 */
	@Input()
	modelProperties: ModelProperty[] = [];

	/**
	 * List of relations of the model class managed by this create CRUD component.
	 */
	@Input()
	modelRelations: ModelRelation[] = [];

	/**
	 * Whether the info sidebar is current displayed.
	 */
	@Input()
	displayViewHistory: boolean = false;

	@Input()
	showHeader: boolean = true;

	@Input()
	showActionButtons: boolean = true;

	/**
	 * Whether to hide the edit button in the view mode
	 */
	@Input()
	hideEditButton: boolean = false;

	/**
	 * When set to true, the create-edit would scroll to invalid input when try to submit
	 */
	@Input()
	scrollToInvalidInput: boolean = true;

	/**
	 * The audits for this model. Only applicable when in edit mode.
	 */
	@Input()
	audits: T[] = [];

	/**
	 * Changes to be made to the original model.
	 */
	pendingChanges: { [key in keyof E]?: any } = {};

	/**
	 * Whether the current page is displayed in admin/backend mode or not.
	 */
	isAdminMode: boolean = false;

	/**
	 * The names of the buttons used to add an additional instance of an entity type to the list of entities in multi crud mode
	 * Each string in this array will create a button that calls the multi entity event emitter to add another entity.
	 */
	addMultiEntityButtonNames: {name: string, many: boolean}[] = [];

	/**
	 * The default multi entity buttons which should be added to the page.  These are added into addMultiEntityButtonNames during initialisation,
	 * and are present so that the buttons are shown when creating a new entity, as the button names are otherwise based on the entity groups present in
	 * the model props and relations.
	 */
	@Input()
	defaultMultiEntityButtons: {name: string, many: boolean}[] = [];

	// % protected region % [Override the default model Properties attributes here] off begin
	/**
	 * A collection of all of the model properties which do not have an assigned group. This list is populated initially in separateForms()
	 */
	modelPropertiesWithNoGroup: { props: ModelProperty[], relations: ModelRelation[], combined: any[]};

	/**
	 * Used to store model properties in groups defined by the user. This list is populated initially in separateForms()
	 */
	modelPropertiesWithGroup: { [s: string]: AttributeGroup } = {};
	// % protected region % [Override the default model Properties attributes here] end

	// % protected region % [Add any additional class fields here] off begin
	// % protected region % [Add any additional class fields here] end

	constructor(
		private store: Store<{ router: RouterState }>,
		// % protected region % [Add any additional constructor parameters here] off begin
		// % protected region % [Add any additional constructor parameters here] end
	) {
		super(
			// % protected region % [Add any additional constructor arguments here] off begin
			// % protected region % [Add any additional constructor arguments here] end
		);

		this.store.select(getRouterState).subscribe(
			routerState => {
				if (routerState && routerState.url) {
					this.isAdminMode = routerState.url.startsWith('/admin');
				}
			}
		);

		// % protected region % [Add any additional constructor logic here] off begin
		// % protected region % [Add any additional constructor logic here] end
	}

	/**
	 * @inheritDoc
	 */
	ngOnInit() {
		// % protected region % [Add any additional ngOnInit logic before the main body here] off begin
		// % protected region % [Add any additional ngOnInit logic before the main body here] end

		this.actionButtonGroups = [
			// % protected region % [Add any additional element groups before the main ones here] off begin
			// % protected region % [Add any additional element groups before the main ones here] end
			{
				elements: [
					// % protected region % [Add any additional elements before the main ones here] off begin
					// % protected region % [Add any additional elements before the main ones here] end

					// % protected region % [Override the default buttons here] off begin
					{
						label: 'Cancel',
						buttonStyle: ButtonStyle.OUTLINE,
						buttonAccentColour: ButtonAccentColour.PRIMARY,
						iconPos: IconPosition.LEFT,
						callback: this.onCancelClicked.bind(this)
					},
					{
						label: 'Edit',
						buttonStyle: ButtonStyle.SOLID,
						buttonAccentColour: ButtonAccentColour.PRIMARY,
						iconPos: IconPosition.LEFT,
						callback: this.onEditClicked.bind(this),
						doHide: () => !this.isDisabled || this.hideEditButton
					},
					{
						label: 'Save',
						buttonStyle: ButtonStyle.SOLID,
						buttonAccentColour: ButtonAccentColour.PRIMARY,
						iconPos: IconPosition.LEFT,
						callback: this.model.id ? this.onSaveClicked.bind(this) : this.onCreateClicked.bind(this),
						doHide: () => this.isDisabled
					},
					// % protected region % [Override the default buttons here] end

					// % protected region % [Add any additional elements after the main ones here] off begin
					// % protected region % [Add any additional elements after the main ones here] end
				]
			},
			...this.customGroups,
			// % protected region % [Add any additional element groups after the main ones here] off begin
			// % protected region % [Add any additional element groups after the main ones here] end
		];

		/** 
		 * Calling separateForm is necessary for setting up the data structures used in the create edit page
		 * it will usually always be called as part of ngOnChanges, but also calling it here guarantees it will
		 * be called on startup and allows us to leave the method as private, as we would otherwise need to call it
		 * whenever we instantiate an instance of this class for testing
		 */
		this.separateForm();

		// % protected region % [Add any additional ngOnInit logic after the main body here] off begin
		// % protected region % [Add any additional ngOnInit logic after the main body here] end
	}

	/**
	 * @inheritDoc
	 */
	ngOnChanges(changes: SimpleChanges): void {
		if (changes.hasOwnProperty('modelProperties') || changes.hasOwnProperty('modelRelations')) {
			this.separateForm();
		}
	}

	/**
	 * Triggered when the user clicks on the `Create` button.
	 */
	onCreateClicked() {
		// % protected region % [Add any additional onCreateClicked logic before the main body here] off begin
		// % protected region % [Add any additional onCreateClicked logic before the main body here] end

		if (this.getChangedValuesAndValidate()) {
			this.createOrSaveEventEmitter.emit({
				isCreate: true,
				additionalEntities: this.populateAdditionalEntities(this.model.className)
			});
		}

		// % protected region % [Add any additional onCreateClicked logic after the main body here] off begin
		// % protected region % [Add any additional onCreateClicked logic after the main body here] end
	}

	/**
	 * Triggered when the user clicks on the `Save` button.
	 */
	onSaveClicked() {
		// % protected region % [Add any additional onSaveClicked logic before the main body here] off begin
		// % protected region % [Add any additional onSaveClicked logic before the main body here] end

		if (this.getChangedValuesAndValidate()) {
			this.createOrSaveEventEmitter.emit({
				isCreate: false,
				payload: this.pendingChanges,
				additionalEntities: this.populateAdditionalEntities(this.model.className)
			});
		}

		// % protected region % [Add any additional onSaveClicked logic after the main body here] off begin
		// % protected region % [Add any additional onSaveClicked logic after the main body here] end
	}

	/**
	 * Triggered when the user clicks on the `Edit` button.
	 */
	onEditClicked() {
		// % protected region % [Add any additional onEditClicked logic before the main body here] off begin
		// % protected region % [Add any additional onEditClicked logic before the main body here] end

		this.switchEditEventEmitter.emit(null);

		// % protected region % [Add any additional onEditClicked logic after the main body here] off begin
		// % protected region % [Add any additional onEditClicked logic after the main body here] end
	}

	/**
	 * Triggered when the user clicks on the `Cancel` button.
	 */
	onCancelClicked() {
		// % protected region % [Add any additional onCancelClicked logic before the main body here] off begin
		// % protected region % [Add any additional onCancelClicked logic before the main body here] end

		this.cancelEventEmitter.emit(null);

		// % protected region % [Add any additional onCancelClicked logic after the main body here] off begin
		// % protected region % [Add any additional onCancelClicked logic after the main body here] end
	}

	/**
	 * Triggered when the user clicks on the `View History` button.
	 */
	onViewHistory() {
		// % protected region % [Add any additional onViewHistory logic before the main body here] off begin
		// % protected region % [Add any additional onViewHistory logic before the main body here] end

		this.displayViewHistory = true;
		this.viewHistoryEventEmitter.emit(null);

		// % protected region % [Add any additional onViewHistory logic after the main body here] off begin
		// % protected region % [Add any additional onViewHistory logic after the main body here] end
	}

	/**
	 * Triggered when the user clicks the 'Remove `entity` from Model' or 'Add new `entity` to Model' buttons.  Used during 
	 * multi crud mode when a many relation is present to communicate the desired changes in the list of models (Adding a new one/removing an existing one)
	 * to the crud edit component so that it can make the changes to the properties present in this component
	 * 
	 * @param entityName The name of the entity which is being added/removed.  Only strictly necessary when adding a new object, as in the use
	 * 						case where multiple many relations are present in the crud edit page, the component will need to be able to determine which
	 * 						entity should be added
	 * @param groupId The id of the group which is to be removed
	 */
	onMultiEntityAction(entityName: string, groupId?: string) {
		// % protected region % [Add any additional onMultiEntityAction logic before the main body here] off begin
		// % protected region % [Add any additional onMultiEntityAction logic before the main body here] end

		this.multiEntityEventEmitter.emit({
			groupId: groupId ? groupId : null,
			entityName: entityName,
		});

		// % protected region % [Add any additional onMultiEntityAction logic after the main body here] off begin
		// % protected region % [Add any additional onMultiEntityAction logic after the main body here] end
	}

	/**
	 * Given the JS type of a property in the current model type, return the appropriate textfield type.
	 *
	 * @returns the appropriate input type
	 */
	getInputType(elementType: string): TextfieldType {
		if (elementType === ElementType.NUMBER) {
			return TextfieldType.NUMBER;
		} else if (elementType === ElementType.PASSWORD) {
			return TextfieldType.PASSWORD;
		} else {
			return TextfieldType.TEXT;
		}
	}

	/**
	 * Get the changed values from the modelFormGroup and update to the pendingChanges
	 */
	private getChangedValuesAndValidate(): boolean {
		// % protected region % [Add any additional getChangedValuesAndValidate logic before the main body here] off begin
		// % protected region % [Add any additional getChangedValuesAndValidate logic before the main body here] end

		// Validates all the inputs
		Object.values(this.modelFormGroup.controls)
			.filter(control => !control.disabled)
			.forEach(formControl => formControl.updateValueAndValidity({emitEvent: true}));

		if (this.modelFormGroup.invalid || !this.validateAdditionalEntities()) {
			if (this.scrollToInvalidInput) {
				this.scrollToError();
			}
			return false;
		}

		if (this.model.id) {
			this.pendingChanges = {};
			Object.entries(this.modelFormGroup.controls)
				.filter(([key, formControl]) => formControl.dirty)
				.forEach(([key, formControl]) => this.pendingChanges[key] = formControl.value);
		} else {
			Object.assign(this.model, this.modelFormGroup.value);
		}

		// % protected region % [Add any additional getChangedValuesAndValidate logic after the main body here] off begin
		// % protected region % [Add any additional getChangedValuesAndValidate logic after the main body here] end

		return true;
	}

	/**
	 * Separate properties and relations into their respective groups.
	 */
	private separateForm() {
		// % protected region % [Add any additional separateForm logic before the main body here] off begin
		// % protected region % [Add any additional separateForm logic before the main body here] end

		// As the button names are otherwise populated from the names of the groups passed in, we need to
		// populate the list with the buttons we know we need, as in a entity create situation, no additional groups will be present
		this.addMultiEntityButtonNames = [];
		this.addMultiEntityButtonNames.push(...this.defaultMultiEntityButtons);

		this.modelPropertiesWithGroup = {}; // This is a dictionary which will be added to later in this method
		this.modelPropertiesWithNoGroup = { // This is a similar object to the values of the previous dictionary.  Doesn't have display name as non grouped attributes do not need one
			props: [],
			relations: [],
			combined: []
		};

		// First we separate all model props into different arrays for attributes with no groups, and then a dictionary which contains an array for
		// each group ID.  This ensures that if we have a mix of grouped and ungrouped entities in the component, then they will all be shown
		this.modelProperties.forEach(prop => {
			if (!prop.group) {
				this.modelPropertiesWithNoGroup.props.push(prop);
			} else {
				// If we haven't come across this group before, then we need to add it to the dictionary
				// Otherwise we just add the current prop to the relevant entry
				if (!this.modelPropertiesWithGroup[prop.group.id]) {
					// Each entry in the dictionary contains all the attributes on the page for that group, so we need to initialise it empty and then add the relevant info
					this.modelPropertiesWithGroup[prop.group.id] = this.getDefaultGroup(prop);
				}
				this.modelPropertiesWithGroup[prop.group.id].props.push(prop);
			}
		});

		// Works the same as for props, but handling relations instead
		Object.values(this.modelRelations).forEach(prop => {
			if (!prop.group) {
				this.modelPropertiesWithNoGroup.relations.push(prop);
			} else {
				// Still need to do this to handle edge case where a group only contains relations, as that group wouldn't exist at this point
				if (!this.modelPropertiesWithGroup[prop.group.id]) {
					this.modelPropertiesWithGroup[prop.group.id] = this.getDefaultGroup(prop);
				}
				this.modelPropertiesWithGroup[prop.group.id].relations.push(prop);
			}
		});

		this.modelPropertiesWithNoGroup.combined = [
			...this.modelPropertiesWithNoGroup.props
				.filter(item => item.name !== 'id' && item.name !== 'created' && item.name !== 'modified')
				.map(prop => {
					return {...prop, isProp: true};
				}),
			...this.modelPropertiesWithNoGroup.relations.map(ref => {
				return {...ref, isProp: false};
			}),
		].sort((a, b) => a.index - b.index); // we only sort once the lists are combined, as sorting prior to this would cause the lists to be sorted separately, so the desired order might not occur

		// We want to combine the props and relations together into a list sorted by their indexes (if they exist)
		// Combining the lists allows for sorting all attributes together, instead of just sorting props, and then sorting relations
		Object.values(this.modelPropertiesWithGroup).forEach(val => {
			val.combined = [
				...val.props
					.filter(item => item.name !== 'id' && item.name !== 'created' && item.name !== 'modified')
					.map(prop => {
						return {...prop, isProp: true};
					}),
				...val.relations.map(ref => {
					return {...ref, isProp: false};
				}),
			].sort((a, b) => a.index - b.index); // we only sort once the lists are combined, as sorting prior to this would cause the lists to be sorted separately, so the desired order might not occur
		});

		// % protected region % [Add any additional separateForm logic after the main body here] off begin
		// % protected region % [Add any additional separateForm logic after the main body here] end
	}

	/**
	 * Scroll to the element in the dom
	 */
	scrollTo(el: Element) {
		if (el) {
			el.scrollIntoView({ behavior: 'smooth' });
		}
	}

	/**
	 * Find the error message and scroll to it
	 */
	scrollToError() {
		const firstElementWithError = document.querySelector('.ng-invalid');
		this.scrollTo(firstElementWithError);
	}

	/**
	 * Return the default group used to begin grouping together model properties and relations
	 * @param prop a model property or model relation which has a group
	 * @returns the default group object used to store all information in the groups dictionary
	 */
	getDefaultGroup(prop: ModelProperty | ModelRelation): AttributeGroup {
		// % protected region % [Modify the default group attributes here] off begin
		return {
			id: prop.group.id,
			displayName: prop.group.displayName,
			model: prop.group.model ? prop.group.model : undefined,
			form: prop.group.form ? prop.group.form : undefined,
			manyRelation: prop.group.manyRelation ? prop.group.manyRelation : false,
			props: [],
			relations: [],
			combined: [],
		};
		// % protected region % [Modify the default group attributes here] end
	}

	/**
	 * Takes the group containing a FormGroup and a model, and assigns the changed values in the FormGroup to the Model
	 *
	 * The main use for this method is in configuring the crud edit page to create/update multiple related entities at once. It is recommended to
	 * use this method to build the related entity with the changes from the form
	 */
	assignRelationValueChanges(group: AttributeGroup) {
		let refModel = {...group.model};
		Object.assign(refModel, group.form.value);
		return refModel;
	}

	/**
	 * Checks that all additional entities which have been added to this crud page are valid before proceeding with the creation/update
	 * We only check groups that have an associated form, as any attributes without an associated form are assumed to be part of the primary model for this page
	 *
	 * @returns true if all additional entities pass validation, false if there is a validation error
	 */
	validateAdditionalEntities(): boolean {
		// Returning false within forEach doesn't return false for the whole function, so we need to track the status and return it after the iterations are complete
		let valid = true; // Default to true, we can then set valid to false if we come across any validation issues

		// As all attributes in the base ModelFormGroup are checked for validity regardless of grouping, we do not need to check groups that
		// do not have an associated form group, as these groups do not represent additional entities
		Object.values(this.modelPropertiesWithGroup).filter( group => group.form).forEach(group => {
			Object.values(group.form.controls)
				.filter(control => !control.disabled)
				.forEach(formControl => formControl.updateValueAndValidity({emitEvent: true}));

			if (group.form.invalid) {
				if (this.scrollToInvalidInput) {
					this.scrollToError();
				}
				valid = false;
			}
		});

		return valid;
	}

	/**
	 * Creates a list of entities which will be returned in addition to the default event payload.  This allows for configuring different crud edit pages
	 * to return different additional entities.
	 *
	 * @param modelName the name of the model to return additional entities for.  This is entity specific, but will account for whether multi entity has
	 * 			has been turned on for any specific relation
	 * @returns a data structure with all of the additional entities added to this crud edit page
	 */
	populateAdditionalEntities(modelName: string) {
		switch (modelName) {
			default:
				return {};
		}
	}

	/**
	 * Check whether the button to add an additional entity of the given type should be disabled
	 *
	 * @param relationName The name of the button.  This is used to check the groups in modelPropertiesWithGroup for any group which has
	 * 							a name starting with the same name.  By default, the button names and the group ids are based on the same
	 * 							attribute in the crud edit page, so if this remains true then this method will work as intended
	 * @param manyRelation Whether the button is associated with a many relation or a ones relation.  We only ever want to disable a ones relation
	 * 							so if this is true we can return false immediately, as the button will never need to be disabled
	 * @returns true if the button should be disabled, and false otherwise.
	 */
	singleEntityRelationAdded(relationName: string, manyRelation: boolean) {
		if (manyRelation) {
			return false;
		}

		return Object.keys(this.modelPropertiesWithGroup).filter(groupId => groupId.startsWith(relationName.toLowerCase() + '_')).length > 0;
	}

	// % protected region % [Add any additional class methods here] off begin
	// % protected region % [Add any additional class methods here] end
}
