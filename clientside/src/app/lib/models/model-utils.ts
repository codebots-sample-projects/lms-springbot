/*
 * @bot-written
 *
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 *
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import {OrderBy, QueryOperation, QueryParams, Where} from '../services/http/interfaces';
import {AbstractModel, Group, ModelProperty, ModelRelation} from './abstract.model';
import * as _ from 'lodash';
import {FormControl, FormGroup} from '@angular/forms';
import { Action } from '@ngrx/store';
import uuid from 'uuid';


// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

/**
 * Sort By the OrderBy
 * Pass in an array of OrderBy, and sort with it
 * @param entities the entities to sort
 * @param orders the array of order, which determine the attributes to order with
 * @return the sorted entities
 */
export function sortByOrders<T extends AbstractModel>(entities: T[], orders: OrderBy[]): T[] {
	// % protected region % [Add any additional methods here before sorting] off begin
	// % protected region % [Add any additional methods here before sorting] end

	 entities = _.orderBy(entities, orders.map(orderBy => orderBy.path), orders.map(orderBy => orderBy.descending ? 'desc' : 'asc'));

	 // % protected region % [Add any additional methods here after sorting] off begin
	 // % protected region % [Add any additional methods here after sorting] end

	 return entities;
}

/**
 * Sort and filter the model collection by the query parameters
 * @param entities entities to sort and filter
 * @param queryParams The query parameters object to sort and filter the entities
 * @return sorted and filtered entities
 */
export function sortAndFilterByQueryParams<T extends AbstractModel>(entities: T[], queryParams: QueryParams): T[] {
	// % protected region % [Add any additional methods here before sorting and filtering] off begin
	// % protected region % [Add any additional methods here before sorting and filtering] end

	entities = sortByOrders(entities, queryParams.orderBy);

	// % protected region % [Add any additional methods here after sorting and filtering] off begin
	// % protected region % [Add any additional methods here after sorting and filtering] end

	return entities;
}

/**
 * Transfer the enum to an aryy of object
 * @param enumType The Enum in the ts
 */
export function enumToArray(enumType): { key: string, value: any }[] {
	return Object.keys(enumType).map(key => (
		{
			key: key,
			value: enumType[key]
		}
	));
}

/**
 * Create a reactive form based
 * @param modelProperties Properties The properties of the model
 * @param modelRelations modelRelations Relations of the model
 * @param fields filters the attributes and relations which will be added to the form group.
 * 		If present, only props and relations with a name contained in this list will be added to the form group
 * @return The form group created for the model
 */
export function createReactiveFormFromModel(
	modelProperties: ModelProperty[],
	modelRelations: ModelRelation[],
	disabled: boolean = false,
	fields ?: string[]): FormGroup {

	const formGroup = new FormGroup({});

	// % protected region % [Add any additional logic before creating the form here] off begin
	modelProperties
		.filter(prop => fields ? fields.includes(prop.name) : true)
		.forEach(modelProperty => {
			formGroup.addControl(modelProperty.name, new FormControl({
				value: undefined,
				disabled: disabled || modelProperty.readOnly
			}, modelProperty.validators));
		});

	modelRelations
		.filter(relation => fields ? fields.includes(relation.name) : true)
		.forEach(relation => {
			formGroup.addControl(relation.name, new FormControl({
				value: undefined,
				disabled: disabled
			}, relation.validators));
		});
	// % protected region % [Add any additional logic before creating the form here] end

	return formGroup;
}

/**
 * Adds the properties and relations for a new entity to the model properties and model relations arrays for a
 * crud edit page in multiple entity mode. This involves creating a group which can be added to all of the props and
 * relations which need to be added.
 *
 * This method is designed to be generic, so it will work for 1:1, 1:M, and M:M relations, and is usable for any entity type in the
 * application.
 *
 * @param form The form group which will be added to the group
 * @param model The model to be added to the group.  If creating a new entity, a new entity should be passed in here instead of a previously created one
 * @param modelType The type of the model.  Type checking is not possible here so it is assumed that modelType is the type for the model input
 * 						The method will most likely not work if this is not the case.
 * @param groupDisplayName The name of the group which will be displayed on the page.  Also used to create the group id
 * @param currentProps The modelProperties of the crud page.  These will be expanded and returned
 * @param currentRels The modelRelations of the crud page.  These will be expanded with the relations already present in the page and returned
 * @param preparedRelations An instance of the model relations to be added to the master list which has already been passed through
 * 								prepareReferenceCollections.  This method performs important tasks to populate the relation dropdowns, but cannot
 * 								be run within the subscription where specific entities are known, as this will cause an endless feedback loop of
 * 								NgRx calls which will cause the site to not function properly until a reload
 * @param manyRelation Whether the group to be added is a many side of a 1:M or M:M relation.  This is used to add buttons to the crud edit page which
 * 							delete the current entity from the list of related entities
 * @returns a tuple with modified versions of the currentProps and currentRelations inputs.  All original array members will be present, but new members
 * 				representing the props and relations of the new entity will be added
 */
export function getNewModelProperties<model extends AbstractModel, modelType extends typeof AbstractModel>(
	form: FormGroup,
	model: model,
	modelType: modelType, // IMPORTANT: modelType must be the type of the model input.  This can't be checked easily but if they do not match the method will not work as intended
	groupDisplayName: string,
	currentProps: ModelProperty[],
	currentRels: ModelRelation[],
	preparedRelations: ModelRelation[],
	manyRelation: boolean = false,
): { modelProperties: ModelProperty[], modelRelations: ModelRelation[]} {
	let props: ModelProperty[] = [...currentProps];
	let rels: ModelRelation[] = [...currentRels];
	let group: Group;

	// Group needs to be identical for all elements which are meant to be in a group, as the method which processes the group will process
	// the first instance of a group with this groups ID, so any differences in subsequent groups with the same id will be ignored
	// Display name is already pascal case
	const camelCasedGroupDisplayName = groupDisplayName.charAt(0).toLowerCase() + groupDisplayName.slice(1);
	group = {
		// This method will be run multiple times through the change detection cycle when initialising for many relations.
		// We need to be able to check for duplicate groups so it is necessary that a model input produces an identical group on different runs
		id: model.id ? camelCasedGroupDisplayName + '_' + model.id : camelCasedGroupDisplayName + '_' + uuid.v4(),
		displayName: groupDisplayName,
		model: model,
		form: form,
		manyRelation: manyRelation,
	};
	group.form.patchValue(model);

	modelType.getProps().forEach(newProp => {
		newProp.group = group;
		let potentialDuplicate = currentProps.find(prop => prop.group && prop.group.id === newProp.group.id && prop.id === newProp.id);
		// We don't want to add a prop if it already exists, but we do want its updated values, so if the attribute has been added previously
		// we overwrite it with the more current version
		if (potentialDuplicate) {
			currentProps[currentProps.indexOf(potentialDuplicate)] = newProp;
		} else {
			props.push(newProp);
		}
	});

	modelType.getRelations().forEach(newRel => {
		newRel.group = group;

		/*
			The fields being set here are all set on the preparedRelations in prepareReferenceCollections.  That method cannot be called
			in places where getNewModelProperties needs to be called (within subscriptions) so we need to populate each relation with the
			fields from a prepared relation of the same type
		 */
		let populatedRelation: ModelRelation = preparedRelations.find(populated => {
			return populated.id === newRel.id;
		});

		newRel.stateConfig = populatedRelation.stateConfig;
		newRel.collection = populatedRelation.collection;
		newRel.searchFunction = populatedRelation.searchFunction;
		newRel.hideElement = populatedRelation.hideElement;

		let potentialDuplicate = currentRels.find(rel => rel.group && rel.group.id === newRel.group.id && rel.id === newRel.id);
		// Same logic holds here as was discussed above in the props loop
		if (potentialDuplicate) {
			currentRels[currentRels.indexOf(potentialDuplicate)] = newRel;
		} else {
			rels.push(newRel);
		}
	});

	return { modelProperties: props, modelRelations: rels };
}

/**
 * Builds and returns an action which will fetch the necessary entities which need to be viewable/editable in multi entity crud mode
 *
 * @param modelType The Type of the entity to be used.  This is used to call static methods of the type we are trying to fetch.
 * 						This is also used to fetch the actions to be used from fetchPrepareReferenceRequirements()
 * @param relationToExclude The relation to exclude from the expands when fetching the entity/entities.  This will generally be the
 * 								entity which is being created or edited by default on this page prior to adding any extra entities.
 * 								This relation is not necessary and can cause issues when it is included
 * @param collectionId The collection of the entities to be fetched. This needs to be initialised prior to the use of this method
 * @param pageSize The number of entities to fetch.  This is a required field for fetching with query, and will either be 1 (if we are expanding
 * 						a ones relation) or the length of the id list for the entity we are expanding on (if we are expanding a many relation)
 * @param targetModelId The id of the base entity if manyRelation is true, and the id of the expanded entity otherwise
 * @param manyRelation Whether the relation being built is a ones or a many relation.  The wheres, targetModelId, and action type will be
 * 						different depending on the value of this
 * @returns an action, which can be dispatched from the store in the crud edit page to fetch the necessary entities
 */
export function multiCrudExtraEntityAction<modelType extends typeof AbstractModel>(
	modelType: modelType,
	relationToExclude: string,
	collectionId: string,
	pageSize: number,
	targetModelId: string,
	manyRelation: boolean,
): Action {
	let actionHelpers: PrepareReferenceRequirements;
	let where: Where[][] = [[]];
	actionHelpers = fetchPrepareReferenceRequirements(modelType.name);

	if (manyRelation) {
		where = [
			[{
				path: relationToExclude + 'Id',
				operation: QueryOperation.EQUAL,
				value: targetModelId,
			}]
		];
	}

	return new actionHelpers.baseAction(
		manyRelation ? actionHelpers.fetchWithQueryAction : actionHelpers.fetchWithId,
		{
			collectionId: collectionId,
			targetModelId: manyRelation ? undefined : targetModelId,
			queryParams: {
				pageSize: pageSize,
				pageIndex: 0,
				expands: modelType.getRelations().filter(rel => rel.id !== relationToExclude).map(
					relation => {
						return {
							name: relation.id,
							fields: ['id', relation.displayName],
						};
					}
				),
				where: where,
			}
		}
	);
}

/**
 * An interface of all of the objects needed to prepare the entity collection in prepareReferenceModels in crud edit tils
 *
 * An instance of this interface will be returned for every case added to the fetchPrepareReferenceRequirements method below.  By default there is
 * a case for each entity
 */
export interface PrepareReferenceRequirements {
	// The action is an entity specific object.  Using this base action, generic code can be written within loops in the prepareReferenceModels method without needing to know the type of the entity
	baseAction: new (...args: any[]) => Action;
	collectionModelsSelector;
	// The action strings correspond to cases in their respective action types enums.  As each entities enum is a different type, these attributes must be strings
	initCollectionAction: string;
	fetchAllAction: string;
	fetchWithId: string;
	fetchWithQueryAction: string;
}

/**
 * Returns objects and data useful for preparing references in crud edit pages.  As the logic required for each entity will be different, this method
 * has one switch case for each entity.  Ideally this should simplify logic in crud edit pages and reduce the code required in prepareReferenceModels, as
 * with this, the references can be prepared in a for each loop, instead of sequentially in one long method
 *
 * @param modelName the name of the model to return the items for.  This is fed into a switch statement immediately
 * @returns an instance of the PrepareReferenceRequirements interface,
 * 		which allows for preparation of references in crud edit pages without needing to know specific entity types
 */
export function fetchPrepareReferenceRequirements(modelName: string): PrepareReferenceRequirements {
	switch (modelName) {
		default: return null;
	}
}

// % protected region % [Add any additional methods here] off begin
// % protected region % [Add any additional methods here] end
